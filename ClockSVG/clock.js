'use strict'

const CENTER_X = 650;
const CENTER_Y = 250;
const DISTANCE = 200;

const ARROW_SEC_LENGTH = 180;
const ARROW_MIN_LENGTH = 150;
const ARROW_HOUR_LENGTH = 130;


let field = document.getElementById('field');


function setUpHour(s) {
    let angle = -60;
    let hours = document.getElementsByClassName('hour');
    for (let i = 0; i < 12; i++) {
        let x = CENTER_X + DISTANCE * cos(angle);
        let y = CENTER_Y + DISTANCE * sin(angle);
        drawHours(s, x, y);
        drawText(s, x, y + 10, i + 1);
        angle += 30;
    }
}

let cos = function getCosDeg(deg) {
    let rad = deg * Math.PI / 180;
    return Math.cos(rad);
}
let sin = function getSinDeg(deg) {
    let rad = deg * Math.PI / 180;
    return Math.sin(rad);
}

function buildTable(s) {
    let table = document.createElementNS('http://www.w3.org/2000/svg', 'use');
    table.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#table');
    table.setAttribute('x', CENTER_X.toString());
    table.setAttribute('y', CENTER_Y.toString());
    s.appendChild(table);
}
function drawHours(s, x, y) {
    let hourC = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    hourC.setAttribute('r', '30');
    hourC.setAttribute('fill', 'red');
    hourC.setAttribute('cx', x);
    hourC.setAttribute('cy', y);
    field.appendChild(hourC);
}
function drawText(s, x, y, number) {
    let hourT = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    hourT.setAttribute('font-size', '40px');
    hourT.setAttribute('text-anchor', "middle");
    hourT.setAttribute('fill', "black");
    hourT.setAttribute('x', x);
    hourT.setAttribute('y', y);
    hourT.textContent = '' + number + '';
    field.appendChild(hourT);
}
function drawArrowsSec(s) {
    let sec = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    let y2 = CENTER_Y - ARROW_MIN_LENGTH;
    sec.setAttribute('x1', CENTER_X.toString());
    sec.setAttribute('y1', CENTER_Y.toString());
    sec.setAttribute('x2', CENTER_X.toString());
    sec.setAttribute('y2', y2.toString());
    sec.setAttribute('id', 'sec');
    sec.setAttribute('stroke', "black");
    sec.setAttribute('stroke-width', "1");
    s.appendChild(sec);
}
function drawArrowsMin(s) {
    let min = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    let y2 = CENTER_Y - ARROW_MIN_LENGTH;
    min.setAttribute('x1', CENTER_X.toString());
    min.setAttribute('y1', CENTER_Y.toString());
    min.setAttribute('x2', CENTER_X.toString());
    min.setAttribute('y2', y2.toString());
    min.setAttribute('id', 'min');
    min.setAttribute('stroke', "black");
    min.setAttribute('stroke-width', "3");
    s.appendChild(min);
}
function drawArrowsHour(s) {
    let hour = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    let y2 = CENTER_Y - ARROW_HOUR_LENGTH;
    hour.setAttribute('x1', CENTER_X.toString());
    hour.setAttribute('y1', CENTER_Y.toString());
    hour.setAttribute('x2', CENTER_X.toString());
    hour.setAttribute('y2', y2.toString());
    hour.setAttribute('id', 'hour');
    hour.setAttribute('stroke', "black");
    hour.setAttribute('stroke-width', "5");
    s.appendChild(hour);
}


function buildClock(s) {
    buildTable(s);
    setUpHour(s);
    drawArrowsSec(s);
    drawArrowsMin(s);
    drawArrowsHour(s);
}
function setSec(sec) {
    let angle = 6;
    let arrowSec = document.getElementById('sec');
    let x2 = CENTER_X + ARROW_SEC_LENGTH * cos(angle * sec);
    let y2 = CENTER_Y + ARROW_SEC_LENGTH * sin((angle) * sec);
    arrowSec.setAttribute('x2', x2.toString());
    arrowSec.setAttribute('y2', y2.toString());
}
function setMin(min, sec) {
    let angle = 6;
    let arrowMin = document.getElementById('min');
    let x2 = CENTER_X + ARROW_MIN_LENGTH * cos(angle * min+(angle/60*sec));
    let y2 = CENTER_Y + ARROW_MIN_LENGTH * sin(angle * min+(angle/60*sec));
    arrowMin.setAttribute('x2', x2.toString());
    arrowMin.setAttribute('y2', y2.toString());
}
function setHour(hour, min) {
    let angle = 30;
    let arrowHour = document.getElementById('hour');
    let x2 = CENTER_X + ARROW_HOUR_LENGTH * cos(angle * hour+(angle/60*min));
    let y2 = CENTER_Y + ARROW_HOUR_LENGTH * sin(angle * hour+(angle/60*min));
    arrowHour.setAttribute('x2', x2.toString());
    arrowHour.setAttribute('y2', y2.toString());
}

function startTime() {
    let currTime = new Date();
    let hours = currTime.getHours() - 3;
    let minutes = currTime.getMinutes() - 15;
    let seconds = currTime.getSeconds() - 15;
    setSec(seconds);
    setMin(minutes, seconds);
    setHour(hours, minutes);
}

buildClock(field);
setInterval(startTime, 1000);














