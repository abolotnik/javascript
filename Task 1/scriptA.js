var person = {};
var regName = /^([А-Я][а-я]+)$/;
var regAge = /^\d{1,2}$/;
var main = document.getElementById('main');
var button5 = document.getElementById('ES5');
var button6 = document.getElementById('ES6');
var buttonReset = document.getElementById('reset');
var fio = document.getElementById('fio');
var age = document.getElementById('age');
var sex = document.getElementById('sex');
var pens = document.getElementById('pens');

function getInfoES5() {
    var agree = confirm('Вы готовы начать?');
    if (agree) {
        getSurName();
        getName();
        getMidName();
        getAge();
        manOrWoman();
        main.style.display = "inline-block";
        button5.style.display = "none";
        button6.style.display = "none";
        buttonReset.style.display = "block";

        fio.innerHTML = "<span>Здравствуйте " + person.surname + " " + person.name + " " + person.midname + "</span>";
        age.innerHTML = "<span>Ваш возраст в годах: " + " " + person.age + "<br>Ваш возраст в днях: " + " " + (person.age * 364) + "</span>";
        sex.innerHTML = "<span>Вы: " + " " + person.sex + "</span>";
        pens.innerHTML = "<span>Вы на пенсии: " + " " + person.pensioner + "</span>";

        console.log("Здравствуйте " + person.surname + " " + person.name + " " + person.midname + "\nВаш возраст в годах: " + " " + person.age + "\nВаш возраст в днях: " + " " + person.age * 364 + "\nВы: " + " " + person.sex + "\nВы на пенсии: " + " " + person.pensioner);
    }
}
function reset() {
    main.style.display = "none";
    button5.style.display = "block";
    button6.style.display = "block";
    buttonReset.style.display = "none";
}
function getName() {
    var name = prompt('Введите Ваше имя ( на пример: Иван)');
    if (!name) {
        alert('Что-то Вы не так вводите!!!')
        getName();
    }
    else {
        person.name = name;
    }
}
function getSurName() {
    var surname = prompt('Введите Вашу фамилию ( на пример: Иванов)');
    console.log(regName.test(surname));
    if (!(surname)) {
        alert('Что-то Вы не так вводите!!!');
        getSurName();
    } else {
        person.surname = surname;
    }
}
function getMidName() {
    var midname;
    midname = prompt('Введите Ваше имя ( на пример: Иванович)');
    console.log(regName.test(midname));
    if (!(midname)) {
        alert('Что-то Вы не так вводите!!!');
        getMidName();
    } else {
        person.midname = midname;
    }
}
function getAge() {
    var age;
    do {
        age = prompt('Введите Ваш возраст в годах ( на пример: 22)');
        console.log(regAge.test(age));
        if (!(regAge.test(age)) || age <= 0) {
            if (age <= 0) {
                alert(`Возраст ${age} какой-то не правильный`);
            } else {
                alert('Что-то Вы не так вводите!!!')
            }
        }
    } while (!regAge.test(age) || age <= 0);
    person.age = age;
    console.log(person.age);
}
function manOrWoman() {
    var man = confirm('Вы мужчина? (если да, нажмите Ok)');
    if (man) {
        person.sex = "мужчина";
        person.age >= 65 ? person.pensioner = 'Да' : person.pensioner = 'Нет';
    } else {
        person.sex = "женщина";
        person.age >= 60 ? person.pensioner = 'Да' : person.pensioner = 'Нет';
    }

}


