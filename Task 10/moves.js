var grab = null;
var xy = {};


function onMouseDown(e,div) {
    console.log("down");
    grab = e.target;

    let s = getComputedStyle(grab);
    let identity = grab.getAttribute('id');
    console.log(grab.getAttribute('id'));
    xy.top = s.top;
    xy.left = s.left;
    xy.mtop = e.clientY;
    xy.mleft = e.clientX;
    let inActiveDiv = document.getElementsByTagName("div");
    for(let i = 0, n = inActiveDiv.length; i < n; i++){
        if(inActiveDiv[i].getAttribute('id')!= identity){
            inActiveDiv[i].style.zIndex = "";
        } else {
            inActiveDiv[i].style.zIndex = 10;
        }
    }
}
function onMouseUp(e, div) {
    console.log("up");
    grab = null;
    xy = {};
}
function onMouseMove(e,div) {
    if (grab) {
        grab.style.top = parseInt(xy.top) + (e.clientY - xy.mtop) + 'px';
        grab.style.left =  parseInt(xy.left) + (e.clientX - xy.mleft) + 'px';
    }
    div.innerHTML = `x:${e.screenX}; y:${e.screenY}`;

}
function onMouseOut(div) {
    grab = null;
    xy = {};
    div.innerHTML = ``;
}

