'use strict'
var hour;
var hourSize = {};
var center = {};

var clock = document.getElementById('clock');
var timer = document.getElementById('timer');
var lineHour;
var lineMin;
var lineSec;
var countSec;

function addHours() {
    for (var i = 0; i < 12; i++) {
        hour = document.createElement('div');
        var x = i + 1;
        hour.setAttribute('id', x.toString());
        hour.setAttribute('class', 'hour');
        hour.innerHTML = x;
        clock.appendChild(hour);
    }
    hourSize.width = hour.offsetWidth;
    hourSize.height = hour.offsetHeight;


}
function setCenter() {
    var c = clock.getBoundingClientRect();
    center.width = c.right - c.left;
    center.height = c.bottom - c.top;
    center.x = center.width / 2;
    center.y = center.height / 2;
    center.distance = center.width / 2 - 40;
}
function setUpHour() {
    var angle = 150;
    var hours = document.getElementsByClassName('hour');
    for (var i = 0, n = hours.length; i < n; i++) {
        let x = center.x + center.distance * cos(angle) - (hourSize.width / 2);
        let y = center.y + center.distance * sin(angle) - (hourSize.height / 2);
        hours[i].style.top = x + "px";
        hours[i].style.left = y + "px";
        angle -= 30;
    }

    countSec = 0;

    lineHour = document.createElement('div');
    lineHour.className = 'lineHour';
    clock.appendChild(lineHour);
    lineHour.style.top = (center.y - 20) + 'px';
    lineHour.style.left = (center.x - 3) + 'px';

    lineMin = document.createElement('div');
    lineMin.className = 'lineMin';
    clock.appendChild(lineMin);
    lineMin.style.top = (center.y - 20) + 'px';
    lineMin.style.left = (center.x - 2) + 'px';

    lineSec = document.createElement('div');
    lineSec.className = 'lineSec';
    clock.appendChild(lineSec);
    lineSec.style.top = (center.y - 20) + 'px';
    lineSec.style.left = (center.x) + 'px';
}
var cos = function getCosDeg(deg) {
    var rad = deg * Math.PI / 180;
    return Math.cos(rad);
}
var sin = function getSinDeg(deg) {
    var rad = deg * Math.PI / 180;
    return Math.sin(rad);
}
function updateTime() {
    var currTime = new Date();
    var hours = currTime.getHours();
    var minutes = currTime.getMinutes();
    var seconds = currTime.getSeconds();
    timer.innerHTML = (hours > 9 ? hours:"0"+hours) + ':' + (minutes > 9?minutes:"0"+minutes) + ':' + (seconds> 9?seconds:"0"+seconds);
    lineSec.style.transform = `rotate(${180+6*(seconds+1+countSec)}deg)`;
    if (seconds == 59)
        countSec +=60;
    lineMin.style.transform = `rotate(${180 + 6 * minutes + Math.ceil(0.1 * seconds)}deg)`;
    lineHour.style.transform = `rotate(${180 + 30 * hours + Math.ceil(0.5 * minutes)}deg)`;
}


setInterval(updateTime, 1000);


setCenter();
addHours();
setUpHour();
