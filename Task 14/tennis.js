var left = document.getElementById("left");
var right = document.getElementById("right");
var count = document.getElementsByTagName("span")[0];
var leftCount = 0;
var rightCount = 0;
var passedRight = false;
var passedLeft = false;
var leftY = {
    startY: 0,
    posY: 125,
    endY: 250,
    lenghtY: 150,
    frontX: 15,
    speedY: 20,
    move: function () {
        left.style.top = this.posY + "px";
    }
}
var rightY = {
    startY: 0,
    posY: 125,
    endY: 250,
    lenghtY: 150,
    frontX: 15,
    speedY: 20,
    move: function () {
        right.style.top = this.posY + "px";
    }
}
var ballH = {
    posX: 275,
    posY: 175,
    speedX: 2,
    speedY: 1,
    width: 50,
    height: 50,
    startPosX: 275,
    startPosY: 175,

    update: function () {
        var ballElem = document.getElementById('ball');
        ballElem.style.left = this.posX + "px";
        ballElem.style.top = this.posY + "px";
    }
}
var areaH = {
    width: 600,
    height: 400
}
var timer;
var speed = 1.2;

function start() {
    // плавное движение - от 25 кадр/сек
    if (timer) {
        clearInterval(timer);
    }
    timer = setInterval(tick, 40);
    setInterval(speedUp, 10000);
}


function passLeft() {
    if (!passedLeft) {
        if (ballH.posX < 15 && ballH.posY >= leftY.posY && ballH.posY <= (leftY.posY + leftY.lenghtY)) {
            passedLeft = true;
            return true;
        }
    }
}
function passRight() {
    if (!passedRight) {
        if ((ballH.posX + ballH.width) > 585 && ballH.posY >= rightY.posY && ballH.posY <= (rightY.posY + rightY.lenghtY)) {
            passedRight = true;
            return true;
        }
    }
}

function tick() {

    ballH.posX += ballH.speedX;
    // вылетел ли мяч правее стены?
    if (passRight()) {
        ballH.speedX = -ballH.speedX;
        ballH.posX = areaH.width - ballH.width - rightY.frontX;
        passedRight = false;
    }
    if (ballH.posX + ballH.width > areaH.width) {
        clearInterval(timer);
        ++leftCount;
        count.innerHTML = "" + leftCount + ":" + rightCount + "";
        ballH.posX = ballH.startPosX;
        ballH.posY = ballH.startPosY;
        ballH.speedX = 2;
        ballH.speedY = 1;
    }
    // вылетел ли мяч левее стены?
    if (passLeft()) {
        ballH.speedX = -ballH.speedX;
        ballH.posX = leftY.frontX;
        passedLeft = false;
    }
    if (ballH.posX < 0) {
        clearInterval(timer);
        ++rightCount;
        count.innerHTML = "" + leftCount + ":" + rightCount + "";
        ballH.posX = ballH.startPosX;
        ballH.posY = ballH.startPosY;
        ballH.speedX = 2;
        ballH.speedY = 1;
    }

    ballH.posY += ballH.speedY;
    // вылетел ли мяч ниже пола?
    if (ballH.posY + ballH.height > areaH.height) {
        ballH.speedY = -ballH.speedY;
        ballH.posY = areaH.height - ballH.height;
    }
    // вылетел ли мяч выше потолка?
    if (ballH.posY < 0) {
        ballH.speedY = -ballH.speedY;
        ballH.posY = 0;
    }
    ballH.update();
}
function speedUp() {
    ballH.speedX *= speed;
    ballH.speedY *=speed;
}

function moveToTop(player) {
    /*if (player.posY <= 0) {
     player.posY = "0";
     }*/
    if (player.posY > 0) {
        player.posY -= player.speedY;
        if (player.posY <= 0) {
            player.posY = 0;
        }
    }
    // setTimeout(moveToDown(player),10);
    player.move();
}
function moveToDown(player) {
    if (player.posY < areaH.height - player.lenghtY) {
        /*if (player.posY > areaH.height - player.lenghtY) {
         player.posY = areaH.height - player.lenghtY;
         }*/
        player.posY += player.speedY;
        if (player.posY >= areaH.height - player.lenghtY) {
            player.posY = areaH.height - player.lenghtY;
        }
    }
    // setTimeout(moveToDown(player),10);
    player.move();
}

ballH.update();
leftY.move();
rightY.move();


document.onkeydown = function (event) {
    console.log(event);
    switch (event.code) {
        case "KeyA" :
            console.log("A");
            setInterval(moveToTop(leftY), 40);
            break;
        case "KeyZ":
            console.log("Z");
            setInterval(moveToDown(leftY), 40);
            break;
        case "ArrowUp":
            console.log("slach");
            setInterval(moveToTop(rightY), 40);
            break;
        case "ArrowDown":
            console.log("quote");
            setInterval(moveToDown(rightY), 40);
            break;
    }
    /*    if (event.code == "KeyA") {
     console.log("A");
     setInterval(moveToTop(left), 40);
     }
     if (event.code == "KeyZ") {
     console.log("Z");
     setInterval(moveToDown(left), 40);
     }
     if (event.code == "Slash") {

     }
     if (event.code == "Quote") {

     }*/
}


