'use strict;'


let m;
let x;
let a = [];

function generateSingleArray(m, x) {
    var aTemp = [];
    for (var i = 0; i < x; i++) {
        aTemp[i] = (Math.floor((Math.random() * (m + 1)) + x));
    }
    console.log(aTemp);
    return aTemp;
}

function changeArray1(m, x) {
    const arr1 = generateSingleArray(m, x);
    let n = arr1.length;
    const odd = n % 2;
    let center = Math.floor(n / 2);
    let temp;
    for (var i = 0; i < center; i++) {
        temp = arr1[i];
        arr1[i] = arr1[i + center + odd];
        arr1[i + center + odd] = temp;
    }
    console.log(arr1);
}

function changeArray2(m, x) {
    const arr2 = generateSingleArray(m, x);
    let n = arr2.length;
    let quarter = Math.floor(n / 4);
    const odd = n % 2;
    let center = Math.floor(n / 2);
    let temp1;
    let temp2;
    for (var i = 0; i < quarter; i++) {
        temp1 = arr2[i];
        temp2 = arr2[n - 1 - i];
        arr2[i] = arr2[n - 1 - center - odd - i];
        arr2[n - 1 - i] = arr2[center + odd + i];
        arr2[n - 1 - center - odd - i] = temp1;
        arr2[center + odd + i] = temp2;
    }
    console.log(arr2);
}

changeArray1(10, 9);
console.log('');
changeArray2(10, 10);

