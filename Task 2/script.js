var person = {};
var regName = /^([А-Я][а-я]+)$/;
var regAge = /^\d{1,2}$/;

function getInfoES5() {
    var agree = confirm('Вы готовы начать?');
    if (agree) {
        getSurName();
        getName();
        getMidName();
        getAge();
        manOrWoman();
        console.log("Здравствуйте "+person.surname+" " +person.name+" "+person.midname+"\nВаш возраст в годах: "+" "+person.age+"\nВаш возраст в днях: "+" "+person.age * 364+"\nВы: "+" "+person.sex +"\nВы на пенсии: "+" "+person.pensioner);
        alert("Здравствуйте "+person.surname+" " +person.name+" "+person.midname+"\nВаш возраст в годах: "+" "+person.age+"\nВаш возраст в днях: "+" "+person.age * 364+"\nВы: "+" "+person.sex +"\nВы на пенсии: "+" "+person.pensioner);
    }
}
function getInfoES6() {
    var agree = confirm('Вы готовы начать?');
    if (agree) {
        getSurName();
        getName();
        getMidName();
        getAge();
        manOrWoman();
        console.log(`Здравствуйте ${person.surname} ${person.name} ${person.midname},\nВаш возраст в годах: ${person.age},\nВаш возраст в днях: ${person.age * 364},\nВы: ${person.sex}\nВы на пенсии: ${person.pensioner}`);
        alert(`Здравствуйте ${person.surname} ${person.name} ${person.midname},\nВаш возраст в годах: ${person.age},\nВаш возраст в днях: ${person.age * 364},\nВы: ${person.sex}\nВы на пенсии: ${person.pensioner}`);
         }
}
function getName() {
    var name;
    do {
        name = prompt('Введите Ваше имя ( на пример: Иван)');
        console.log(regName.test(name));
        if (!(regName.test(name))) {
            alert('Что-то Вы не так вводите!!!')
        }
    } while (!regName.test(name));
    person.name = name;
}
function getSurName() {
    var surname;
    do {
        surname = prompt('Введите Вашу фамилию ( на пример: Иванов)');
        console.log(regName.test(surname));
        if (!(regName.test(surname))) {
            alert('Что-то Вы не так вводите!!!')
        }
    } while (!regName.test(surname));
    person.surname = surname;
}
function getMidName() {
    var midname;
    do {
        midname = prompt('Введите Ваше имя ( на пример: Иванович)');
        console.log(regName.test(midname));
        if (!(regName.test(midname))) {
            alert('Что-то Вы не так вводите!!!')
        }
    } while (!regName.test(midname));
    person.midname = midname;
}
function getAge() {
    var age;
    do {
        age = prompt('Введите Ваш возраст в годах ( на пример: 22)');
        console.log(regAge.test(age));
        if (!(regAge.test(age)) || age <= 0) {
            if (age <= 0) {
                alert(`Возраст ${age} какой-то не правильный`);
            } else {
                alert('Что-то Вы не так вводите!!!')
            }
        }
    } while (!regAge.test(age) || age <= 0);
    person.age = age;
    console.log(person.age);
}
function manOrWoman() {
    var man = confirm('Вы мужчина? (если да, нажмите Ok)');
    if (man) {
        person.sex = "мужчина";
        person.age >= 65 ? person.pensioner = 'Да' : person.pensioner = 'Нет';
    } else {
        person.sex = "женщина";
        person.age >= 60 ? person.pensioner = 'Да' : person.pensioner = 'Нет';
    }

}


