'use strict';

const readline = require('readline');
const result = {
    name:'',
    surname:'',
    midname:'',
    age:0,
    sex:''
}

function askName() {
    const rl = prompt();
    rl.question('Какое Ваше имя? ', (answer) => {
        rl.close();
        if (!answer) {
            console.log('Имя не может быть пустым.');
            askName();
        }
        else {
            result.name = answer;
            askMidname();
        }
    });
}
function askMidname() {
    const rl = prompt();
    rl.question('Какой Ваше отчество? ', (answer) => {
        rl.close();
        if(!answer){
            console.log('Отчество не может быть пустым.');
            askMidname();
        } else {
            result.midname = answer;
            askSurname();
        }
    });
}
function askSurname() {
    const rl = prompt();
    rl.question('Какая у Вас фамилия? ', (answer) => {
        rl.close();
        if(!answer){
            console.log('Фамилия не может быть пустой.');
            askSurname();
        } else {
            result.surname = answer;
            askSex();
        }

    });
}
function askSex() {
    const rl = prompt();
    rl.question('Вы мужчиан? (если да, то введите любой символ)', (answer) => {
        rl.close();
        if(!answer){
            result.sex = 'женщина';
        } else {
            result.sex = 'мужчина';
        }
        askAge();

    });
}
function askAge() {
    const rl = prompt();
    rl.question('Какой Ваш возраст? ', (answer) => {
        rl.close();
        const age = +answer;
        if (isNaN(age)) {
            console.log('Возраст должен быть введен числом.');
            askAge();
        }
        else {
            result.age = age;
            if(result.sex ==='женщина'){
                if(result.age >=60){
                    result.pancion = 'пенсионного возраста';
                } else{
                    result.pancion = 'не пенсионного возраста';
                }
            } else {
                if(result.age >=65){
                    result.pancion = 'пенсионного возраста';
                } else{
                    result.pancion = 'не пенсионного возраста';
                }
            }

            end();
        }
    });
}
function prompt() {
    return readline.createInterface({ input: process.stdin,  output: process.stdout});
}
function end() {
    console.log(`Ваши данные: ${result.name} ${result.midname} ${result.surname}\n Возраст в годах: ${result.age}\n Возраст в днях: ${result.age*364}\n Вы: ${result.sex} ${result.pancion}`);
}
askName();
