function Person(name) {
    this._name = name;
    this._friends = [];
    this._spouse = null;
}
Person.prototype.sayhello = function () {
    var s = "hi, my name's " + this._name;
    if (this._friends.length) {
        s = s + "\nMy friends:"
        for (var i = 0, n = this._friends.length; i < n; i++) {
            s = s + "\n" + this._friends[i]._name;
        }
    }
    if (this._spouse) {
        s = s + "\nСупруг(а): " + this._spouse._name;
        // console.log('У меня ' + this._friends.length + ' друзей');
    }
    console.log(s);
}


function Man(name) {
    Person.call(this, name);
    this.setFriend = function (person) {
        if (person instanceof Man) {
            console.log(this._name + ' подружился с ' + person._name);
            this._friends.push(person);
            if (!(this in person._friends)) {
                person._friends.push(this);
            }
        } else {
            console.log(this._name + ' не может дружить с ' + person._name);
        }
    }
    this.married = function (person) {
        if (person instanceof Woman) {
            if(!this._spouse) {
                this._spouse = (person);
                person._spouse = (this);
            }
        } else {
            console.log(this._name + ' не может жениться на ' + person._name);
        }
    }
}
function Woman(name) {
    Person.call(this, name);
    this.setFriend = function (person) {
        if (person instanceof Woman) {
            this._friends.push(person);
            person._friends.push(this);
        } else {
            console.log(this._name + ' не может дружить с ' + person._name);
        }
    }

    this.married = function (person) {
        if (person instanceof Man) {
            if (!person._spouse) {
                this._spouse = (person);
                person._spouse = this;
            }
        } else {
            console.log(this._name + ' не может выйти за ' + person._name);
        }
    }
}

var f = function () {
};
f.prototype = Person.prototype;

Man.prototype = new f();
Woman.prototype = new f();

Man.prototype.constructor = Man;
Woman.prototype.constructor = Woman;


var r = new Man('rick');
var s = new Man('Sick');
var z = new Woman('Zizy');

r.sayhello();
console.log('-------------');
r.setFriend(s);
r.married(z);
r.sayhello();
console.log('-------------');
z.sayhello();
console.log('-------------');
s.sayhello();





