'use strict;'

class Person {
    constructor(name) {
        this._name = name;
        this._friends = [];
        this._spouse = null;
    }

    sayHello() {
        var s = "hi, my name's " + this._name;
        if (this._friends.length) {
            s = s + "\nMy friends:"
            for (var i = 0, n = this._friends.length; i < n; i++) {
                s = s + "\n" + this._friends[i]._name
            }
        }
        if (this._spouse) {
            s = s + "\nСупруг(а): " + this._spouse._name;
            console.log('У меня ' + this._friends.length + ' друзей');
        }
        console.log(s);
    }
}
class Man extends Person {
    constructor(name) {
        super(name);
    }

    setFriend(person) {
        if (person instanceof Man) {
            if (person in this._friends) {
                this._friends.push(person);
                person._friends.push(this);
            }
        } else {
            console.log(this._name + ' не может дружить с ' + person._name);
        }
    }

    maryMe(person) {
        if (!this._spouse) {
            console.log('1');
            if (this instanceof Man && person instanceof Woman) {
                this._spouse = person;
                person._spouse = this;
                console.log(this._name + ' cупруг (а)' + person._name);
            }
        } else {
            console.log(this._name + ' уже женат на ' + this._spouse._name);
        }
    }
}
class Woman extends Person {
    constructor(name) {
        super(name);
    }

    setFriend(person) {
        if (!(this in person._friends)) {
            if (person instanceof Woman) {
                this._friends.push(person);
                person._friends.push(this);
            } else {
                console.log(this._name + ' не может дружить с ' + person._name);
            }
        }
    }

    maryMe(person) {
        if (!(this._spouse)) {
            console.log('1');
            if (this instanceof Woman && person instanceof Man) {
                this._spouse = person;
                person._spouse = this;
            } else {
                console.log('2');
            }
        } else {
            console.log(this._name + ' уже замужем за ' + this._spouse._name);
        }
    }
}

var a = new Man('Man');
var k = new Woman('Woman');
var l = new Man('Man');
var m = new Woman('Woman');
var h = new Woman('Woman');
var f = new Woman('Woman');

k.sayHello();
k.setFriend(a);
k.setFriend(a);
k.setFriend(h);
k.setFriend(f);
k.setFriend(m);
k.maryMe(a);
a.maryMe(k);
k.sayHello();






