"use strict;"

class Drinks {
    constructor() {
        this.drink = {};
    }

    addValue(key, value) {
        this.drink[key] = value;
    }

    getValue(key) {
        return this.drink[key];
    }

    deleteValue(key) {
        delete this.drink[key];
    }

    getKeys() {
        return Object.keys(this.drink);
    }
}

class Service {


    constructor() {
    }

    startNew() {
        let buttons = document.getElementsByClassName("button-wrapper")[0];
        let form = document.getElementById("wrapper");
        buttons.style.display = "none";
        form.style.display = "block";
    }

    addIngredient(e) {
        e.preventDefault();
        let ingredients = document.getElementsByClassName("ingredients")[0];
        console.log(ingredients.childNodes);
        ingredients.appendChild(ingredients.childNodes[1].cloneNode(true));
    }

    delIngredient(e) {
        e.preventDefault();
        let ingredients = document.getElementsByClassName("ingredients")[0];
        let n = ingredients.childNodes.length;
        console.log(ingredients.childNodes);
        if (ingredients.childNodes.length > 5) {
            ingredients.removeChild(ingredients.childNodes[n - 1]);
        }
    }

    endWrite() {
        let buttons = document.getElementsByClassName("button-wrapper")[0];
        let form = document.getElementById("wrapper");
        buttons.style.display = "";
        form.style.display = "";
    }

    writeRecipie(e) {
        e.preventDefault();
        let alco;
        if (document.getElementById("alco").checked) {
            alco = "алкогольный";
        } else {
            alco = "безалкогольный";
        }
        let title = document.getElementById("title").value;
        let ingredients = document.getElementsByName("ingredients")[0].value;
        let recipie = document.getElementsByTagName("textarea")[0].value;
        let drink = {};
        drink.alco = alco;
        drink.ingred = ingredients;
        drink.recipies = recipie;
        if (drinkStorage.getValue(title) == null) {
            drinkStorage.addValue(title, drink);
        } else {
            alert('Такой напиток есть в базе!');
        }
        console.log(drinkStorage);
        service.endWrite();
    }

    deleteDrink(e) {
        // e.preventDefault();
        let title;
        do {
            title = prompt("Введите название напитка");
        } while (title.length === 0);
        drinkStorage.deleteValue(title);
    }

    getdrink(e) {
        e.preventDefault();
        let title;
        do {
            title = prompt("Введите название напитка");
        } while (title.length === 0);
        if (drinkStorage.getValue(title)) {
            alert(title + ': ' + '\n' + 'алкогольный: ' + drinkStorage.getValue(title)['alco'] + '\n' + 'Ингридиенты: '+ drinkStorage.getValue(title)['ingred'].toString()+ '\n' + 'рецепт приготовления: ' + drinkStorage.getValue(title)['recipies']);
        } else {
            alert('напиток не существует');
        }
    }

    getAllDrinks(e){
        let keys=drinkStorage.getKeys();
        if (keys.length===0){
            alert('напитков нет');
        }else{
            let s='';
            for (let i=0;i<keys.length;i++){
                s=s+keys[i]+'\n';
            }
            alert(s);
        }
    }


}

var drinkStorage = new Drinks();
var service = new Service();












