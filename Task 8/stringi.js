let string = prompt('введите строку');
let str = string.split('');
console.log(str);
let newArrayByForEach = [];
let pattern=new RegExp(/[аеёиоуыэюя]/);
str.forEach(function (v) {
    if (!v.search(/[аеёиоуыэюя]/)) {
        newArrayByForEach.push(v);
    }
});
let newArrayByFilter = str.filter(function (v) {
    return !v.search(/[аеёиоуыэюя]/);
});
let newStringByReduce=str.reduce(function FRA(R, V) {
    if(!pattern.test(R)){
        R='';
    }
    if(!pattern.test(V)){
        V='';
    }
    return R+V;
});
console.log(`С функцией Reduce - ${newStringByReduce.length}`);
console.log(`С функцией Filter - ${newArrayByFilter.length}`);
console.log(`С функцией ForEach - ${newArrayByForEach.length}`);